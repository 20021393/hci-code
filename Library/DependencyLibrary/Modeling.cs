using System.Collections.Generic;
namespace DependencyLibrary.Goals {
    public interface ThingToDo<C,N,E,A,R,D,I> {
        C Id {get;}
        N Title {get;}
        E Description {get;}
        I AdditionalInformation {get;}
        System.Collections.Generic.IEnumerable<D> Dependencies {get;}
        A Status {get;}
        IEnumerable<R> Requirements {get;}
    }
    public interface Notebook<T,C,N,E,A,R,U> where T:ThingToDo<C,N,E,A,R,C,U> {
        System.Collections.Generic.IEnumerable<T> ThingsToDo {get;}
    }
}